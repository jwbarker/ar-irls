clear

%% Simulate data
Fs = 10; % sampling rate

load sample_data_10Hz.mat % this a few channels of resting state data at 10 Hz
y = y*1e6; % scale the data; it might in mol/L

% simulate a design matrix
X = zeros( size(y,1), 1 );

% 15 stim events every 15s
onset = (15:15:225)'*Fs;
X(onset) = 1;

% canonical hrf
a1 = 6;
a2 = 16;
b1 = 1;
b2 = 1;
c = 1/6;

t = 0:1/Fs:32; t = t';

hrf = b1^a1*t.^(a1-1).*exp(-b1*t)/gamma(a1) - c*b2^a2*t.^(a2-1).*exp(-b2*t)/gamma(a2);

% convolve stim with hrf
X = filter(hrf, 1, X);
X = X/max(X);

% add response to data
y = y + X*(3*ones(1,size(y,2))); % all same magnitude

% optionally, add some drift terms
% legendre polynomials
ord = 5;
t = linspace(-1,1,size(X,1))';
L = zeros(size(X,1),ord);
L(:,1) = 1;
L(:,2) = t;
for n = 2:ord+1
    L(:,n+1) = ( (2*n+1) * L(:,n) .* t - n * L(:,n-1) ) / (n+1);
end

% make sure to add at least a mean regressor (i.e. the 0th order polynomial
% term)
X = [X L];

%% This illustrates use of code
% S is struct with coefficients, covariance, and other useful info.
% The last argument is the maximum AR model order; 3 or 4 times the
% sampling rate is good for this parameter
S = ar_irls(y,X,4*Fs);

% first column is X is the predicted hemodynamic response
% the coefficient:
S.beta(1,:)' % correct beta is 2

% the tstats:
S.tstat(1,:)'

% pval (two-sided)
S.pval(1,:)'

%% Plot some stuff

% The algorithm basically does three steps:
% 1) find an optimal whitening filter
% 2) whiten y and X
% 3) perform iterative reweighted least squares (robustfit in matlab)
%
% the whitening proces makes the motion easier to identify and corrects the
% serial correlations from blood pressure, cardiac, and respiratory signals

% lets look at some channels
for iCH = [2 5 9]
    figure

    subplot(3,1,1)
    plot(y(:,iCH))
    title('data')

    % this whitens the data
    f = [1; -S.a{iCH}(2:end)]; % whitening filter from AR model
    yf = filter(f, 1, y(:,iCH)); 

    subplot(3,1,2)
    plot(yf)
    title('whitened data') % note that motion is easy to identify here

    subplot(3,1,3)
    plot(S.w(:,iCH))
    title('IRLS Weights') % note that during visible artifacts weigth drops to 0
end